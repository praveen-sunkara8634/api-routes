from django.apps import AppConfig


class GlidesConfig(AppConfig):
    name = 'glides'
